
const fs = require('fs');
const path = require('path');

String.prototype.toTemplate=function() {
  return eval('`'+this+'`');
};

function calcFilePath(stack, fileName) {
  if( fileName[ 0 ] == '/' ) // If got full path, do nothing
    return fileName;
  
  let result = '';
  if( stack[ 0 ] && path.isAbsolute(stack[ 0 ]) ) {
    // do nothing
  } else 
    result = process.cwd() + '/';  
  
  for(let item of stack) {
    let curDir = path.dirname(item);
    
    if( curDir != '.' )
      result += curDir + '/';
  }
  
  result += fileName;
  result = path.normalize(result);
  
  return result;
}

global.include = function(fileName) {
  if( !global.include.stack )
    global.include.stack = [];
  
  let realPath = calcFilePath(global.include.stack, fileName);
  global.include.stack.push(fileName);
  
  let inputHTML = fs.readFileSync(realPath, { encoding: 'utf8' });
  let result = inputHTML.toTemplate();
  
  global.include.stack.pop();
  return result;
};

global.get = function(key) {
  return global[ key ];
};

global.set = function(key, value) {
  global[ key ] = value;
  return '';
};